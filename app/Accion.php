<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Accion extends Model
{
    protected $table='tbl_accion';
    protected $primaryKey='id';
    protected $fillable=['nombre','conclucion','postura','vivienda'];
    protected $hidden = ['created_at', 'updated_at'];
}
