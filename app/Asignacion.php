<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Asignacion extends Model
{
    protected $table='tbl_asignacion';
    protected $fillable=[
        'credito',
        'nombre',
        'calle',
        'colonia',
        'delegacion',
        'municipio',
        'cp',
        'saldoActual',
        'regimenActual',
        'omisos',
        'mensualidadSegmento',
        'importeRegularizar',
        'seguroActual',
        'seguroOmisos',
        'mesesDisponibles',
        'stm',
        'bcn',
        'dcp',
        'fpp1',
        'fpp2',
        'fpp3',
        'fpp4',
        'fpp5',
        'fpp6',
        'fpp7',
        'fpp8',
        'fpp9',
        'perfilamiento',
        'created_at',
        'updated_at'
    ];
    public static function delegaciones(){
        return DB::select('SELECT delegacion,COUNT(*) as total FROM tbl_asignacion GROUP BY delegacion');
    }
}
