<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Atiende extends Model
{
    protected $table='tbl_atiende';
    protected $primaryKey='id';
    protected $fillable=['nombre','vivienda'];
    protected $hidden = ['created_at', 'updated_at'];
}