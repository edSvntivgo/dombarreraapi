<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conclucion extends Model
{
    protected $table='tbl_conclucion';
    protected $id='id';
    protected $fillable=[
        'nombre','postura','vivienda'
    ];
    protected $hidden = [
        'created_at', 'updated_at',
    ];
}
