<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Jobs\TelefonosJob;

class Kernel extends ConsoleKernel
{
    protected $commands = [
        //
    ];
    
    protected function schedule(Schedule $schedule)
    {
        $schedule->job(new TelefonosJob)->everyMinute();
    }

    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
