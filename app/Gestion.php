<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Imagen;

class Gestion extends Model
{
    protected $table='tbl_gestion';
    protected $primaryKey='id';
    protected $fillable=[
        'usuario',
        'credito',
        'nombre',
        'delegacion',
        'regimenActual',
        'vivienda',
        'atiende',
        'postura',
        'conclucion',
        'accion',
        'latitud',
        'longitud',
        'hora_inicio',
        'hora_fin',
        'fecha_gestion',
        'email',
        'telefono',
        'foto',
        'comentario'
    ];
    protected $hidden=[
        'created_at','updated_at'
    ];

    public static function guardar($request,$path){
            date_default_timezone_set('America/Mexico_City');
            $asignacion =Asignacion::where('credito',$request->credito)
                ->select('nombre','delegacion','regimenActual')
            ->get();
            foreach ($asignacion as  $value) {
               $nombre=$value->nombre;
               $delegacion=$value->delegacion;
               $regimenActual=$value->regimenActual;
            }
            
            $gestion=new Gestion();
            $gestion->usuario = $request->usuario;
            $gestion->credito = $request->credito;
            $gestion->nombre=$nombre;
            $gestion->delegacion=$delegacion;
            $gestion->regimenActual=$regimenActual;
            $gestion->vivenda = $request->vivienda;
            $gestion->atiende = $request->atiende;
            $gestion->postura = $request->postura;
            $gestion->conclucion = $request->conclucion;
            $gestion->accion = $request->accion;
            $gestion->latitud = $request->latitud;
            $gestion->longitud = $request->longitud;
            $gestion->hora_inicio = $request->hora_inicio;
            $gestion->hora_fin = $request->hora_fin;
            $gestion->fecha_gestion=$request->fecha_gestion;
            $gestion->foto = null;
            $gestion->telefono =$request->telefono;
            $gestion->email=$request->email;
            $gestion->comentario=$request->comentario;
            $gestion->created_at=date('Y-m-d H:i:s');
            $gestion->save();
            
            $imagen = new Imagen();
            $imagen->imagen=$path;
            $imagen->id_gestion=$gestion->id;
            $imagen->fecha_gestion=date('Y-m-d');
            $imagen->created_at=date('Y-m-d H:i:s');
            $imagen->save();

            return true;
    }
}
