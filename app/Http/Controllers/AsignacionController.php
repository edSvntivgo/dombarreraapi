<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Asignacion;

class AsignacionController extends Controller
{
    public function credito(Request $request)
    {
        $data=Asignacion::where('credito',$request->credito)->get();
        if(count($data)>0){
            return response()->json($data,201);
        }else{
            return response()->json($data,404);
        }
    }
    public function allAsignacion(){
        $data=Asignacion::all();
        $total=count($data);
        return response()->json($total);
    }
    public function asignacionDelegacion(){
        $delegaciones=Asignacion::delegaciones();
        return response()->json($delegaciones);
    }
    public function syncAsignacion(Request $request){
        ini_set('memory_limit', '-1');
        $data=Asignacion::all();
        return response()->json($data,200);
    }
    public function zona(Request $request)
    {
        $data=Asignacion::where('municipio','like','%'.$request->zona.'%')->get();
        return response()->json($data,200);
    }
}
