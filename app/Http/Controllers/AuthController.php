<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;

class AuthController extends Controller
{
    /*Registro de usuario*/
    public function signUp(Request $request)
    {
        $request->validate([
            'username'=>'required|string|unique:users',
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string',
            'zona'=>'required|string',
        ]);

        User::create([
            'username'=>$request->username,
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'zona'=>$request->zona
        ]);

        return response()->json([
            'message' => 'Successfully created user!'
        ], 201);
    }

    /* Inicio de sesión*/
    public function login(Request $request)
    {
        $request->validate([
            'username' => 'required|string',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);

        $credentials = request(['username', 'password']);
        if (!\Auth::attempt($credentials))
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);

        $user = $request->user();
        $tokenResult = $user->createToken('Token '.$request->username);

        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse($token->expires_at)->toDateTimeString()
        ]);
    }
    /*Cierre de Session*/
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->json(['message'=>"Successfully logged out"]);
    }
    /*Verificar Usuario*/
    public function user(Request $request){
        return response()->json($request->user());
    }
}