<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gestion;
use App\Telefono;
use DB;

class GestionController extends Controller
{
    public function save(Request $request){
        $image_64 ='data:image/png;base64'.$request->foto;
        $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];
        $imageName = rand(1,100000000).'.'.$extension;
        $path="fotos/".$imageName;
        \Storage::disk('fotos')->put($imageName, base64_decode($request->foto));
        $response=Gestion::guardar($request,$path);
        if($request->telefono!=null){
            $valida_telefono=Telefono::validar($request);
        }
        if($response === true){
            return response()->json("Succes create!",201);
        }else{
            return response()->json("Error al guardar gestion",406);
        }
    }
    public function gestiones(Request $request){
        date_default_timezone_set('America/Mexico_City');
        $data=Gestion::where('usuario',$request->username)
            ->whereRaw("DATE(created_at)=CURDATE()")
            ->count();
        return response()->json($data,201);
    }
}
