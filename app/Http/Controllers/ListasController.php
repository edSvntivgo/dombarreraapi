<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Accion;
use App\Atiende;
use App\Postura;
use App\Conclucion;
use App\Vivienda;

class ListasController extends Controller
{
    public function vivienda(Request $request){
        $data = Vivienda::all();
        return response()->json($data,201);
    }
    public function atiende(Request $request){
        $data = Atiende::where('vivienda','like','%'.$request->vivienda.'%')->get();
        return response()->json($data,201);
    }
    public function syncAtiende(Request $request){
        $data = Atiende::all();
        return response()->json($data,201);
    }
    public function postura(Request $request){
        $data = Postura::where('vivienda','like','%'.$request->vivienda.'%')
                    ->where('atiende','like','%'.$request->atiende.'%')
                    ->get();
        return response()->json($data,201);
    }
    public function syncPostura(Request $request){
        $data = Postura::all();
        return response()->json($data,201);
    }
    public function conclucion(Request $request){
        $data = Conclucion::where('vivienda','like','%'.$request->vivienda.'%')
                    ->where('postura','like','%'.$request->postura.'%')
                    ->get();
        return response()->json($data,201);
    }
    public function syncConclucion(Request $request){
        $data = Conclucion::all();
        return response()->json($data,201);
    }
    public function accion(Request $request){
        $data = Accion::where('vivienda','like','%'.$request->vivienda.'%')
                    ->where('postura','like','%'.$request->postura.'%')
                    ->where('conclucion','like','%'.$request->conclucion.'%')
                    ->get();
        return response()->json($data,201);
    }
    public function syncAccion(Request $request){
        $data = Accion::all();
        return response()->json($data,201);
    }
}