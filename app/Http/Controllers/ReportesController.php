<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reportes;
use App\Gestion;
use DB;

class ReportesController extends Controller
{
    public function save(Request $request){
        $response=Reportes::guardar($request);
        if($response === true){
            return response()->json("Succes create!",201);
        }else{
            return response()->json("Error al guardar gestion",406);
        }
    }
    public function consulta(Request $request){
        if(strlen($request->credito)>=9){
            $gestiones=Gestion::where('credito',$request->credito)->count();
            if($gestiones>0){
                $consulta=Gestion::where('credito',$request->credito)->paginate(10);
                return response()->json(['message'=>'Informacion Encontrada','status'=>201,'data'=>$consulta]);
            }else{
                return response()->json(['message'=>'El credito no tiene gestiones domiciliarias','status'=>406]);
            }
        }else{
            return response()->json(['message'=>'El credito debe ser mayor o igual a 9 digitos','status'=>406]);
        }
    }
    public function acceso(Request $r){
        $data=DB::select('call conteo_visitas(?,?)',[$r->f1,$r->f2]);
        return response()->json($data);
    }
}
