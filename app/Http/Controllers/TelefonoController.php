<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Telefono;
use App\TelefonoNuevo;

class TelefonoController extends Controller
{
    public function valida(Request $r){
        $data=TelefonoNuevo::where('usuario',$r->usuario)
            ->where('created_at','like','%'.date('Y-m').'%')
            ->select('id_telefono_nuevo','credito','telefono','validado')
            ->get();
        return response()->json($data);
    }
}
