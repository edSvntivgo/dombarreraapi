<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Imagen extends Model
{
    protected $table='tbl_imagen';
    protected $primariKey='id';
    protected $fillable=[
        'id',
        'imagen',
        'id_gestion',
        'fecha_gestion',
        'created_at',
        'updated_at'
    ];
}
