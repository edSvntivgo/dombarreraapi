<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\TelefonoNuevo;
use App\TelefonoValidado;

class TelefonosJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data=TelefonoNuevo::where('created_at','like','%'.date('Y-m-d').'%')->get();
        foreach ($data as $i) {
            $t=new TelefonoValidado();
            $t->usuario=$i->usuario;
            $t->credito=$i->credito;
            $t->telefono=$i->telefono;
            $t->validado=$i->validado;
            $t->save();
        }
        return $data;
    }
}
