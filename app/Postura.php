<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Postura extends Model
{
    protected $table='tbl_postura';
    protected $primariKey='id';
    protected $fillable=[
        'nombre','atiende','vivienda'
    ];
    protected $hidden=[
        'created_at','updated_at'
    ];
}
