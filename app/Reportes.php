<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reportes extends Model
{
    protected $table="tbl_reportes";
    protected $primaryKey='id';
    protected $fillable=[
        'usuario',
        'app_name',
        'fecha_reporte'
    ];
    protected $hidden=[
        'created_at',
        'updated_at'
    ];

    public static function guardar($request){
        $reporte=new Reportes();
        $reporte->usuario=$request->usuario;
        $reporte->app_name=$request->app_name;
        $reporte->fecha_reporte=date('Y-m-d');
        $reporte->save();
        return true;
    }
}
