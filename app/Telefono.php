<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\TelefonoNuevo;
class Telefono extends Model
{
    protected $connection ='telefononico';
    protected $table='tbl_gestion_infonavit_telefonica';
    protected $primaryKey='ID';
    protected $fillable=[
        'CVE_CREDITO_TELEFONO',
        'NUM_CREDITO',
        'NUM_TELEFONO',
        'CVE_TIPO_RED',
        'NUM_TIPO_CONTACTO',
        'NOM_TIPO_CONTACTO',
        'FCH_GESTION',
        'HRA_GESTION',
        'NOM_MITROL_MEJOR',
        'FCH_MITROL_MEJOR',
        'HRA_MITROL_MEJOR',
        'DES_MITROL_CAUSAQ850',
        'NOM_MITROL_UPD',
        'FCH_MITROL_UPD',
        'HRA_MITROL_UPD',
        'NOM_MITROL_DISCADOR',
        'NUM_MITROL_MARCACION',
        'NUM_MITROL_CONTESTA',
        'NUM_MITROL_CONECTA',
        'NUM_MITROL_NOCONECTA',
        'NUM_TELEFONO_LN',
        'NUM_TELEFONO_ADD',
        'FCH_REGISTRO',
        'FCH_ACTUALIZA'
    ];
    public static function validar($r){
        $q=Telefono::select();
        if(!empty($r->telefono)){
            $q->where('NUM_TELEFONO',$r->telefono)
                ->where('NUM_TELEFONO_LN',0)
                ->where('NUM_TELEFONO_ADD',1);
        }
        $existe=$q->count();
        if($existe>0){
            $data=['usuario'=>$r->usuario,'credito'=>$r->credito,'telefono'=>$r->telefono,'validado'=>1];
            $validacion=TelefonoNuevo::where('credito',$r->credito)->where('telefono',$r->telefono)->count();
            if($validacion==0){
                TelefonoNuevo::create($data);
            }
        }else{
            $data=['usuario'=>$r->usuario,'credito'=>$r->credito,'telefono'=>$r->telefono,'validado'=>0];
            $validacion=TelefonoNuevo::where('credito',$r->credito)->where('telefono',$r->telefono)->count();
            if($validacion==0){
                TelefonoNuevo::create($data);
            }
        }
    }
}
