<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TelefonoNuevo extends Model
{
    protected $table='tbl_telefonos_nuevos';
    protected $primaryKey='id';
    protected $fillable=[
        'usuario',
        'credito',
        'telefono',
        'validado',
        'created_at',
        'updated_at',
    ];
    
}
