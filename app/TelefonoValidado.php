<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TelefonoValidado extends Model
{
    protected $connection ='telefononico';
    protected $table='tbl_telefonos_nuevos_dombarrera';
    protected $primaryKey='id';
    protected $fillable=[
        'usuario',
        'credito',
        'telefono',
        'validado',
        'created_at',
        'updated_at'
    ];
}
