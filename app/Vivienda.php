<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vivienda extends Model
{
    protected $table='tbl_vivienda';
    protected $primaryKey='id';
    protected $fillable=[
        'nombre'
    ];
    protected $hidden=[
        'created_at','updated_at'
    ];
}
