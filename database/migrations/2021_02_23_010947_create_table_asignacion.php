<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAsignacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_asignacion', function (Blueprint $table) {
            $table->bigInteger('credito')->unique()->prmary();
            $table->string('nombre',100)->nullable();
            $table->string('calle',10000)->nullable();
            $table->string('colonia',100)->nullable();
            $table->string('delegacion',100)->nullable();
            $table->string('municipio',100)->nullable();
            $table->string('cp',10)->nullable();
            $table->string('saldoActual',50)->nullable();
            $table->string('regimenActual',50)->nullable();
            $table->string('omisos',2)->nullable();
            $table->string('mensualidadSegmento',50)->nullable();
            $table->string('importeRegularizar',50)->nullable();
            $table->string('seguroActual',50)->nullable();
            $table->string('seguroOmisos',50)->nullable();
            $table->string('mesesDisponibles',50)->nullable();
            $table->string('stm',50)->nullable();
            $table->string('bcn',50)->nullable();
            $table->string('dcp')->nullable();
            $table->string('fpp1')->nullable();
            $table->string('fpp2')->nullable();
            $table->string('fpp3')->nullable();
            $table->string('fpp4')->nullable();
            $table->string('fpp5')->nullable();
            $table->string('fpp6')->nullable();
            $table->string('fpp7')->nullable();
            $table->string('fpp8')->nullable();
            $table->timestamps();
            $table->index(['credito']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_asignacion');
    }
}
