<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGestion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_gestion', function (Blueprint $table) {
            $table->increments('id');
            $table->string('usuario',80);
            $table->string('credito',10);
            $table->string('vivenda',70);
            $table->string('atiende',70);
            $table->string('postura',70);
            $table->string('conclucion',70);
            $table->string('accion',70);
            $table->string('latitud',25);
            $table->string('longitud',25);
            $table->time('hora_inicio')->nullable();
            $table->time('hora_fin')->nullable();
            $table->string('email')->nullable();
            $table->string('telefono')->nullable();
            $table->text('foto');
            $table->text('comentario');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_gestion');
    }
}
