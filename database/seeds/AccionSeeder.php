<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Str;

class AccionSeeder extends Seeder
{
    public function run()
    {
        DB::table('tbl_accion')->truncate();
        DB::table('tbl_accion')->insert([
            "nombre"=>"Marcar",
            "conclucion"=>"Ningun Resultado",
            "postura"=>"Sin Postura Sin Contacto",
            "vivienda"=> ",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,Abandonado,Deshabitado,Habita en Vacaciones,Vandalizado,Inaccesible,No Corresponde Domicilio Alterno,Habitado,",
            "data_token"=>Str::random(4),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Reinspeccionar Fin de Semana",
            "conclucion"=>"Ningun Resultado",
            "postura"=>"Sin Postura Sin Contacto",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,Inaccesible,Habitado,",
            "data_token"=>Str::random(4),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Reinspeccionar Nocturno",
            "conclucion"=>"Ningun Resultado",
            "postura"=>"Sin Postura Sin Contacto",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,Inaccesible,Habitado,",
            "data_token"=>Str::random(4),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=> "Reinspeccionar Por La Mañana",
            "conclucion"=> "Ningun Resultado",
            "postura"=> "Sin Postura Sin Contacto",
            "vivienda"=> ",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,Inaccesible,Habitado,",
            "data_token"=>Str::random(4),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Reinspeccionar Por Lar Tarde",
            "conclucion"=>"Ningun Resultado",
            "postura"=>"Sin Postura Sin Contacto",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,Inaccesible,Habitado,",
            "data_token"=>Str::random(4),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Validar Pago",
            "conclucion"=>"Exhibe Pago",
            "postura"=>"Aclaracion de Convenio",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,",
            "data_token"=>Str::random(4),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Validar Pago",
            "conclucion"=>"Exhibe Pago",
            "postura"=>"Aclaracion de Pagos",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,",
            "data_token"=>Str::random(4),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Remitir Infonavit",
            "conclucion"=>"Ningun Resultado",
            "postura"=>"Actitud Hostil",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,",
            "data_token"=>Str::random(4),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Invitar a meddiacion",
            "conclucion"=>"Negativa de Pago",
            "postura"=>"Actitud Hostil",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Invitar a meddiacion",
            "conclucion"=>"Negativa de Pago",
            "postura"=>"Actitud Hostil",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Validacion de Tramite",
            "conclucion"=>"Acredita Defuncion",
            "postura"=>"Defuncion TT",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Validar Documentos",
            "conclucion"=>"Acredita Defuncion",
            "postura"=>"Defuncion TT",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Levantar N° caso INFONATEL",
            "conclucion"=>"Posible Defuncion",
            "postura"=>"Defuncion TT",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Recabar Documentos",
            "conclucion"=>"Posible Defuncion",
            "postura"=>"Defuncion TT",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Recabar Pago",
            "conclucion"=>"Se Aplico Convenio",
            "postura"=>"Acepto BCN",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Validar Pago",
            "conclucion"=>"Se Aplico Convenio",
            "postura"=>"Acepto BCN",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Recabar Pago",
            "conclucion"=>"Se Aplico Convenio",
            "postura"=>"Acepto DCP",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Validar Pago",
            "conclucion"=>"Se Aplico Convenio",
            "postura"=>"Acepto DCP",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Recabar Pago",
            "conclucion"=>"Se Aplico Convenio",
            "postura"=>"Acepto STM",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Validar Pago",
            "conclucion"=>"Se Aplico Convenio",
            "postura"=>"Acepto STM",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Recabar Pago",
            "conclucion"=>"Se Aplico FPP",
            "postura"=>"Acepto FPP",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Validar Pago",
            "conclucion"=>"Se Aplico FPP",
            "postura"=>"Acepto FPP",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Validacion de Tramite",
            "conclucion"=>"Acredita Invalidez",
            "postura"=>"Invalidez TT",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Validar Documentos",
            "conclucion"=>"Acredita Invalidez",
            "postura"=>"Invalidez TT",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Levantar N° caso INFONATEL",
            "conclucion"=>"Posible Invalidez",
            "postura"=>"Invalidez TT",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Recabar Documentos",
            "conclucion"=>"Posible Invalidez",
            "postura"=>"Invalidez TT",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Invitar a Mediación",
            "conclucion"=>"Ningun Resultado",
            "postura"=>"Negativa de Pago",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Marcar",
            "conclucion"=>"Ningun Resultado",
            "postura"=>"Negativa de Pago",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Remitir INFONAVIT",
            "conclucion"=>"Ningun Resultado",
            "postura"=>"Negativa de Pago",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Reinspeccionar Supervisor",
            "conclucion"=>"Ningun Resultado",
            "postura"=>"Negativa de Pago",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Marcar",
            "conclucion"=>"Promesa de Pago",
            "postura"=>"Promesa de Pago",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Recabar Pago",
            "conclucion"=>"Promesa de Pago",
            "postura"=>"Promesa de Pago",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Validar Pago",
            "conclucion"=>"Promesa de Pago",
            "postura"=>"Promesa de Pago",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([

        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Marcar",
            "conclucion"=>"Posible Pago de Omisos",
            "postura"=>"Promesa de Pago Omisos",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Recabar Pago",
            "conclucion"=>"Posible Pago de Omisos",
            "postura"=>"Promesa de Pago Omisos",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Validar Pago",
            "conclucion"=>"Posible Pago de Omisos",
            "postura"=>"Promesa de Pago Omisos",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Marcar",
            "conclucion"=>"Posible Pago de Omisos",
            "postura"=>"Promesa de Regularizacion",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Recabar Pago",
            "conclucion"=>"Posible Pago de Omisos",
            "postura"=>"Promesa de Regularizacion",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Validar Pago",
            "conclucion"=>"Posible Pago de Omisos",
            "postura"=>"Promesa de Regularizacion",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Sin Accion",
            "conclucion"=>"Posible Pago  en Parcialidades",
            "postura"=>"Promesa de Pago en Parcialidades",
            "vivienda"=>",Conyuge_Concubina,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Marcar",
            "conclucion"=>"Posible Liquidacion",
            "postura"=>"Promesa de Liquidacion",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Recabar Pago",
            "conclucion"=>"Posible Liquidacion",
            "postura"=>"Promesa de Liquidacion",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Validar Pago",
            "conclucion"=>"Posible Liquidacion",
            "postura"=>"Promesa de Liquidacion",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Confirmar Cita",
            "conclucion"=>"Cita",
            "postura"=>"Quiere Convenio",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Marcar",
            "conclucion"=>"Posible Convenio",
            "postura"=>"Quiere Convenio",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Visitar Domicilio Garantia",
            "conclucion"=>"Posible Convenio",
            "postura"=>"Quiere Convenio",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Solicitar Baja Convenio INFONAVIT",
            "conclucion"=>"Posible Convenio_Sin Produccion",
            "postura"=>"Quiere Convenio",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=> "Solicitar Convenio INFONAVIT",
            "conclucion"=> "Posible Convenio_Sin Produccion",
            "postura"=> "Quiere Convenio",
            "vivienda"=> ",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Reporta Pago",
            "conclucion"=>"Liquido Credito",
            "postura"=>"Reporta Pago",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Reporta Pago",
            "conclucion"=>"Pago Convenio",
            "postura"=>"Reporta Pago",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Reporta Pago",
            "conclucion"=>"Pago en Parcialidades",
            "postura"=>"Reporta Pago",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Reporta Pago",
            "conclucion"=>"Pago FPP",
            "postura"=>"Reporta Pago",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Reporta Pago",
            "conclucion"=>"Regularizo Credito",
            "postura"=>"Reporta Pago",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Marcar",
            "conclucion"=>"Ningun Resultado",
            "postura"=>"Sin Interes",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Reinspeccionar Fin De Semana",
            "conclucion"=>"Ningun Resultado",
            "postura"=>"Sin Interes",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Reinspeccionar Nocturno",
            "conclucion"=>"Ningun Resultado",
            "postura"=>"Sin Interes",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Reinspeccionar Por La Mañana",
            "conclucion"=>"Ningun Resultado",
            "postura"=>"Sin Interes",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Reinspeccionar Por La Tarde",
            "conclucion"=>"Ningun Resultado",
            "postura"=>"Sin Interes",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Reinspeccionar Supervisor",
            "conclucion"=>"Ningun Resultado",
            "postura"=>"Sin Interes",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,Inaccesible,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Validar Pago",
            "conclucion"=>"Posible Pago",
            "postura"=>"Credito en Roa",
            "vivienda"=>",Conyuge_Concubina,Habita Familiar,Habita Titular,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Marcar",
            "conclucion"=>"Posible Pago  en Parcialidades",
            "postura"=>"Promesa de Pago en Parcialidades",
            "vivienda"=>",Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Recabar Pago",
            "conclucion"=>"Posible Pago  en Parcialidades",
            "postura"=>"Promesa de Pago en Parcialidades",
            "vivienda"=>",Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Validar Pago",
            "conclucion"=>"Posible Pago  en Parcialidades",
            "postura"=>"Promesa de Pago en Parcialidades",
            "vivienda"=>",Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Buscar Domicilio Alterno",
            "conclucion"=>"Ningun Resultado",
            "postura"=>"Sin Postura Sin Contacto",
            "vivienda"=>",Abandonado,Deshabitado,Habita en Vacaciones,Vandalizado,No Corresponde Domicilio Alterno,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Busqueda en Paginas Blancas",
            "conclucion"=>"Ningun Resultado",
            "postura"=>"Sin Postura Sin Contacto",
            "vivienda"=>",Abandonado,Deshabitado,Habita en Vacaciones,Vandalizado,No Corresponde Domicilio Alterno,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Buscar Domicilio Alterno",
            "conclucion"=>"Domicilio Ilocalizable",
            "postura"=>"Sin Postura Sin Contacto",
            "vivienda"=>",Ilocalizable,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Busqueda de Paginas Blancas",
            "conclucion"=>"Domicilio Ilocalizable",
            "postura"=>"Sin Postura Sin Contacto",
            "vivienda"=>",Ilocalizable,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Validar Domicilio Garantia",
            "conclucion"=>"Domicilio Ilocalizable",
            "postura"=>"Sin Postura Sin Contacto",
            "vivienda"=>",Ilocalizable,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Validar Domicilio Alterno",
            "conclucion"=>"Ningun Resultado",
            "postura"=>"Sin Postura Sin Contacto",
            "vivienda"=>",No Corresponde Domicilio Alterno,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Validar Domicilio Alterno",
            "conclucion"=>"Ningun Resultado",
            "postura"=>"Proporciona Domicilio Alterno de TT",
            "vivienda"=>",Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_accion')->insert([
            "nombre"=>"Visitar Domicilio Alterno",
            "conclucion"=>"Ningun Resultado",
            "postura"=>"Proporciona Domicilio Alterno de TT",
            "vivienda"=>",Invadido,Rentado,Traspaso,",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
