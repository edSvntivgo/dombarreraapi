<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Str;

class AtiendeSeeder extends Seeder
{
    public function run()
    {
        DB::table('tbl_atiende')->truncate();
        DB::table('tbl_atiende')->insert([
            'nombre'=>'Administrador',
            'vivienda'=>'Conyuge_Concubina,Habita Familiar,Abandonado,Deshabitado,Habita en Vacaciones,Vandalizado,Inaccesible,No Corresponde Domicilio Alterno,Habitado,Habita Titular',
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_atiende')->insert([
            'nombre'=>'Conyuge_Concubina',
            'vivienda'=>'Conyuge_Concubina,Habita Familiar,Habita Titular',
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_atiende')->insert([
            'nombre'=>'Familiar',
            'vivienda'=>'Conyuge_Concubina,Habita Familiar,Habita Titular',
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_atiende')->insert([
            'nombre'=>'Sin Contacto',
            'vivienda'=>'Conyuge_Concubina,Habita Familiar,Abandonado,Deshabitado,Habita en Vacaciones,Vandalizado,Ilocalizable,Inaccesible,No Corresponde Domicilio Alterno,Habitado,Habita Titular',
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_atiende')->insert([
            'nombre'=>'Vecino',
            'vivienda'=>'Conyuge_Concubina,Habita Familiar,Abandonado,Deshabitado,Habita en Vacaciones,Vandalizado,Inaccesible,No Corresponde Domicilio Alterno,Habitado,Habita Titular',
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_atiende')->insert([
            'nombre'=>'Vigilante',
            'vivienda'=>'Conyuge_Concubina,Habita Familiar,Abandonado,Deshabitado,Habita en Vacaciones,Vandalizado,Inaccesible,No Corresponde Domicilio Alterno,Habitado,Habita Titular',
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_atiende')->insert([
            'nombre'=>'Titular',
            'vivienda'=>'Habita Titular',
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_atiende')->insert([
            'nombre'=>'Invasor',
            'vivienda'=>'Invadido,Rentado,Traspaso',
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_atiende')->insert([
            'nombre'=>'Ocupante',
            'vivienda'=>'Invadido,Rentado,Traspaso,No Corresponde Domicilio Alterno',
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_atiende')->insert([
            'nombre'=>'Apoderado',
            'vivienda'=>'Invadido,Rentado,Traspaso',
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_atiende')->insert([
            'nombre'=>'Sin Postura Sin Contacto',
            'vivienda'=>'Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,Abandonado,Deshabitado,Habita en Vacaciones,Vandalizado,Inaccesible,No Corresponde Domicilio Alterno,Habitado',
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
