<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Str;

class ConclucionSeeder extends Seeder
{
    public function run()
    {
        DB::table('tbl_conclucion')->truncate();
        DB::table('tbl_conclucion')->insert([
            "nombre"=>"Ningun Resultado",
            "postura"=>"Sin Postura Sin Contacto,Actitud Hostil,Negativa de Pago,Sin Interes",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,Abandonado,Deshabitado,Habita en Vacaciones,Vandalizado,Inaccesible,No Corresponde Domicilio Alterno,Habitado",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_conclucion')->insert([
            "nombre"=>"Exhibe Pago",
            "postura"=>"Aclaracion de Convenio,Aclaracion de Pagos,",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar,Habita Titular",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_conclucion')->insert([
            "nombre"=>"Negativa de Pago",
            "postura"=>"Actitud Hostil,",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar,Habita Titular",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_conclucion')->insert([
            "nombre"=>"Acredita Defuncion",
            "postura"=>"Defuncion TT,",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_conclucion')->insert([
            "nombre"=>"Posible Defuncion",
            "postura"=>"Defuncion TT,",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_conclucion')->insert([
            "nombre"=>"Se Aplico Convenio",
            "postura"=>"Acepto BCN,Acepto DCP,Acepto STM,",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_conclucion')->insert([
            "nombre"=>"Se Aplico FPP",
            "postura"=>"Acepto FPP,",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_conclucion')->insert([
            "nombre"=>"Acredita Invalidez",
            "postura"=>"Invalidez TT,",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar,Habita Titular",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_conclucion')->insert([
            "nombre"=>"Posible Invalidez",
            "postura"=>"Invalidez TT,",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar,Habita Titular",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_conclucion')->insert([
            "nombre"=>"Promesa de Pago",
            "postura"=>"Promesa de Pago,",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_conclucion')->insert([
            "nombre"=>"Posible Pago de Omisos",
            "postura"=>"Promesa de Pago Omisos,Promesa de Regularizacion,",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_conclucion')->insert([
            "nombre"=>"Posible Pago  en Parcialidades",
            "postura"=>"Promesa de Pago en Parcialidades,",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_conclucion')->insert([
            "nombre"=>"Posible Liquidacion",
            "postura"=>"Posible Liquidacion,",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_conclucion')->insert([
            "nombre"=>"Cita",
            "postura"=>"Quiere Convenio,",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_conclucion')->insert([
            "nombre"=>"Posible Convenio",
            "postura"=>"Quiere Convenio,",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_conclucion')->insert([
            "nombre"=>"Posible Convenio_Sin Produccion",
            "postura"=>"Quiere Convenio,",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_conclucion')->insert([
            "nombre"=>"Liquido Credito",
            "postura"=>"Reporta Pago,",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_conclucion')->insert([
            "nombre"=>"Pago Convenio",
            "postura"=>"Reporta Pago,",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_conclucion')->insert([
            "nombre"=>"Pago en Parcialidades",
            "postura"=>"Reporta Pago,",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_conclucion')->insert([
            "nombre"=>"Pago FPP",
            "postura"=>"Reporta Pago,",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_conclucion')->insert([
            "nombre"=>"Regularizo Credito",
            "postura"=>"Reporta Pago,",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_conclucion')->insert([
            "nombre"=>"Posible Pago",
            "postura"=>"Credito en Roa,",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar,Habita Titular",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_conclucion')->insert([
            "nombre"=>"Ningun Resultado",
            "postura"=>"Proporciona Domicilio Alterno de TT,",
            "vivienda"=>"Invadido,Rentado,Traspaso",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_conclucion')->insert([
            "nombre"=>"Domicilio Ilocalizable",
            "postura"=>"Sin Postura Sin Contacto,",
            "vivienda"=>"Ilocalizable",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
