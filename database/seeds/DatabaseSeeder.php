<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            AtiendeSeeder::class,
            AccionSeeder::class,
            ConclucionSeeder::class,
            PosturaSeeder::class,
            ViviendaSeeder::class
        ]);
    }
}
