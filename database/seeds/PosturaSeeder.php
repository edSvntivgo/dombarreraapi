<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Str;

class PosturaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_postura')->truncate();
        DB::table('tbl_postura')->insert([
            "nombre"=>"Aclaracion de Convenio",
            "atiende"=>"Administrador,Conyuge_Concubina,Familiar,Sin Contacto,Vecino,Vigilante,Titular",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar,Habita Titular",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_postura')->insert([
            "nombre"=>"Actitud Hostil",
            "atiende"=>"Administrador,Conyuge_Concubina,Familiar,Sin Contacto,Vecino,Vigilante,Titular",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar,Habita Titular",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_postura')->insert([
            "nombre"=>"Defuncion TT",
            "atiende"=>"Administrador,Conyuge_Concubina,Familiar,Sin Contacto,Vecino,Vigilante",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_postura')->insert([
            "nombre"=>"Acepto BCN",
            "atiende"=>"Administrador,Conyuge_Concubina,Familiar,Sin Contacto,Vecino,Vigilante,Titular,Invasor,Ocupante,Apoderado",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_postura')->insert([
            "nombre"=>"Acepto DCP",
            "atiende"=>"Administrador,Conyuge_Concubina,Familiar,Sin Contacto,Vecino,Vigilante,Titular,Invasor,Ocupante,Apoderado",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_postura')->insert([
            "nombre"=>"Acepto STM",
            "atiende"=>"Administrador,Conyuge_Concubina,Familiar,Sin Contacto,Vecino,Vigilante,Titular,Invasor,Ocupante,Apoderado",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_postura')->insert([
            "nombre"=>"Acepto FPP",
            "atiende"=>"Administrador,Conyuge_Concubina,Familiar,Sin Contacto,Vecino,Vigilante,Titular,Invasor,Ocupante,Apoderado",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_postura')->insert([
            "nombre"=>"Invalidez TT",
            "atiende"=>"Administrador,Conyuge_Concubina,Familiar,Sin Contacto,Vecino,Vigilante,Titular",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar,Habita Titular",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_postura')->insert([
            "nombre"=>"Negativa de Pago",
            "atiende"=>"Administrador,Conyuge_Concubina,Familiar,Sin Contacto,Vecino,Vigilante,Titular,Invasor,Ocupante,Apoderado",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_postura')->insert([
            "nombre"=>"Promesa de Pago",
            "atiende"=>"Administrador,Conyuge_Concubina,Familiar,Sin Contacto,Vecino,Vigilante,Titular,Invasor,Ocupante,Apoderado",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_postura')->insert([
            "nombre"=>"Promesa de Pago Omisos",
            "atiende"=>"Administrador,Conyuge_Concubina,Familiar,Sin Contacto,Vecino,Vigilante,Titular,Invasor,Ocupante,Apoderado",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_postura')->insert([
            "nombre"=>"Promesa de Regularizacion",
            "atiende"=>"Administrador,Conyuge_Concubina,Familiar,Sin Contacto,Vecino,Vigilante,Titular,Invasor,Ocupante,Apoderado",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_postura')->insert([
            "nombre"=>"Promesa de Pago en Parcialidades",
            "atiende"=>"Administrador,Conyuge_Concubina,Familiar,Sin Contacto,Vecino,Vigilante,Titular,Invasor,Ocupante,Apoderado",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_postura')->insert([
            "nombre"=>"Promesa de Liquidacion",
            "atiende"=>"Administrador,Conyuge_Concubina,Familiar,Sin Contacto,Vecino,Vigilante,Titular,Invasor,Ocupante,Apoderado",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_postura')->insert([
            "nombre"=>"Quiere Convenio",
            "atiende"=>"Administrador,Conyuge_Concubina,Familiar,Sin Contacto,Vecino,Vigilante,Titular,Invasor,Ocupante,Apoderado",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_postura')->insert([
            "nombre"=>"Reporta Pago",
            "atiende"=>"Administrador,Conyuge_Concubina,Familiar,Sin Contacto,Vecino,Vigilante,Titular,Invasor,Ocupante,Apoderado",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_postura')->insert([
            "nombre"=>"Sin Interes",
            "atiende"=>"Administrador,Conyuge_Concubina,Familiar,Sin Contacto,Vecino,Vigilante,Titular,Invasor,Ocupante,Apoderado",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_postura')->insert([
            "nombre"=>"Credito en Roa",
            "atiende"=>"Administrador,Conyuge_Concubina,Familiar,Sin Contacto,Vecino,Vigilante,Titular",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar,Habita Titular",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_postura')->insert([
            "nombre"=>"Aclaracion de Pagos",
            "atiende"=>"Administrador,Conyuge_Concubina,Familiar,Sin Contacto,Vecino,Vigilante,Titular",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar,Habita Titular",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_postura')->insert([
            "nombre"=>"Sin Postura Sin Contacto",
            "atiende"=>"Sin Contacto",
            "vivienda"=>"Ilocalizable,Inaccesible",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_postura')->insert([
            "nombre"=>"Proporciona Domicilio Alterno de TT",
            "atiende"=>"Invasor,Ocupante,Apoderado",
            "vivienda"=>"Invadido,Rentado,Traspaso",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_postura')->insert([
            "nombre"=>"Sin Postura Sin Contacto",
            "atiende"=>"Administrador,Conyuge_Concubina,Familiar,Sin Contacto,Vecino,Vigilante,Titular,Invasor,Ocupante,Apoderado",
            "vivienda"=>"Conyuge_Concubina,Habita Familiar,Habita Titular,Invadido,Rentado,Traspaso,Abandonado,Deshabitado,Habita en Vacaciones,Vandalizado,Inaccesible,No Corresponde Domicilio Alterno,Habitado",
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}