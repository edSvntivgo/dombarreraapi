<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Str;

class ViviendaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_vivienda')->insert([
            'nombre' =>'Conyuge_Concubina',
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_vivienda')->insert([
            'nombre' =>'Habita Familiar',
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_vivienda')->insert([
            'nombre' =>'Habita Titular',
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_vivienda')->insert([
            'nombre' =>'Invadido',
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_vivienda')->insert([
            'nombre' =>'Rentado',
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_vivienda')->insert([
            'nombre' =>'Traspaso',
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_vivienda')->insert([
            'nombre' =>'Abandonado',
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_vivienda')->insert([
            'nombre' =>'Abandonado',
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_vivienda')->insert([
            'nombre' =>'Deshabitado',
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_vivienda')->insert([
            'nombre' =>'Habita en Vacaciones',
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_vivienda')->insert([
            'nombre' =>'Vandalizado',
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_vivienda')->insert([
            'nombre' =>'Ilocalizable',
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_vivienda')->insert([
            'nombre' =>'Inaccesible',
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_vivienda')->insert([
            'nombre' =>'No Corresponde Domicilio Alterno',
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tbl_vivienda')->insert([
            'nombre' =>'Habitado',
            "data_token"=>Str::random(20),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
