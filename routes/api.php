<?php

use Illuminate\Http\Request;

Route::group(['prefix'=>'auth'],function(){
    Route::post('login','AuthController@login');
    Route::post('signup','AuthController@signUp');
    Route::group(['middleware'=>'auth:api'],function(){
        Route::get('logout','AuthController@logout');
        Route::get('user','AuthController@user');
        Route::group(['prefix' => 'credito'], function() {
            Route::get('zona/{zona}','AsignacionController@zona');
            Route::get('{credito}','AsignacionController@credito');
        });
        Route::group(['prefix' => 'lista'], function() {
            Route::get('vivienda','ListasController@vivienda');
            Route::get('atiende','ListasController@atiende');
            Route::get('postura','ListasController@postura');
            Route::get('conclucion','ListasController@conclucion');
            Route::get('accion','ListasController@accion');
        });
        Route::group(['prefix'=>'sync'],function(){
            Route::get('total','AsignacionController@allAsignacion');
            Route::get('atiende','ListasController@syncAtiende');
            Route::get('postura','ListasController@syncPostura');
            Route::get('conclucion','ListasController@syncConclucion');
            Route::get('accion','ListasController@syncAccion');
            Route::get('asignacion','AsignacionController@syncAsignacion');
            Route::get('delegaciones','AsignacionController@asignacionDelegacion');
        });
        Route::group(['prefix'=>'guardar'],function(){
            Route::get('gestiones','GestionController@gestiones');
            Route::post('gestion','GestionController@save');
        });
        Route::group(['prefix'=>'valida'],function(){
            Route::get('telefono','TelefonoController@valida');
        });
        Route::post('reporte','ReportesController@save');
        Route::get('consulta/{credito}','ReportesController@consulta');
        
    });
});